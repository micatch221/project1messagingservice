const {Datastore} = require('@google-cloud/datastore')
const express = require('express')
const cors = require('cors')

const app = express();
app.use(express.json())
app.use(cors())

const datastore = new Datastore()


app.post('/messages',async (req,res)=>{
    const message = req.body;
    const key = datastore.key('message');
    message.time = new Date();
    const response = await datastore.save({key:key,data:message})
    res.status(201)
    res.send("Successfully Created a new Message")
    console.log(response)
});

app.get('/messages',async (req,res)=>{
    const query = datastore.createQuery('message');
    const [data,metaInfo] = await datastore.runQuery(query)
    res.send(data)
})

app.get('/messages/:mid',async (req,res)=>{
    const key = datastore.key(['message',Number(req.params.mid)]);
    const response = await datastore.get(key);
    res.send(response[0])
})

app.get('/messages/:recipient')

app.get('/messages/:sender')

app.get('message/:recipient/:sender')

app.listen(3000, ()=>console.log('Application started'))

